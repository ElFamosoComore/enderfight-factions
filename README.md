# Bienvenue sur le guide d'intégration des quêtes sur FactionsUUID.

## Étape 1: Modifier la version de Java du projet par une version >= à la 1.8

## Étape 2: Ajouter le package `quests` (disponible dans ce repo) dans `com.massivecraft.factions`

## Étape 3: Faire initialiser les quêtes au démarrage du plugin et faire sauvegarder les données à l'arrêt du plugin.

### Sous-étape 1: Initialiser au démarrage du plugin

Dans la méthode `P#onEnable`, il faut ajouter :
```java
try {
        new QuestManager();
} catch (QuestInitializationException e) {
        e.printStackTrace();
}
```
juste avant la ligne `this.loadSuccessful = true;`

### Sous-étape 2: Sauvegarder les données à l'arrêt du plugin.

Dans la méthode `P#onDisable`, il faut ajouter :

```java
try {
        QuestManager.INSTANCE.saveProfiles();
} catch (QuestSaveException e) {
        e.printStackTrace();
}
```
juste avant la ligne `super.onDisable();`

## Étape 4: Configurer les inventaires via les classes dans le package `quests.config` et modifier les quêtes dans les énumérations dans le package`quests.enums` ayant un nom finissant par `Quests`.
