package com.massivecraft.factions.quests.commands;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.cmd.CommandContext;
import com.massivecraft.factions.cmd.CommandRequirements;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.quests.commands.inventories.MainMenuProvider;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.zcore.util.TL;
import org.bukkit.entity.Player;

public class CmdQuest extends FCommand {
    public CmdQuest() {
        this.aliases.add("quest");

        this.requirements = new CommandRequirements.Builder(Permission.CHAT)
                .memberOnly()
                .withRole(Role.ADMIN)
                .build();
    }

    @Override
    public void perform(CommandContext context) {
        Player player = context.player;
        FPlayer fPlayer = context.fPlayer;

        MainMenuProvider.getInventory(fPlayer).open(player);
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }
}
