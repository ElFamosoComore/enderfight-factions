package com.massivecraft.factions.quests.commands.inventories;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.quests.config.JoinTypeMenuConfig;
import com.massivecraft.factions.quests.config.MainMenuConfig;
import com.massivecraft.factions.quests.enums.QuestType;
import com.massivecraft.factions.quests.inventories.ClickableItem;
import com.massivecraft.factions.quests.inventories.SmartInventory;
import com.massivecraft.factions.quests.inventories.content.InventoryContents;
import com.massivecraft.factions.quests.inventories.content.InventoryProvider;
import com.massivecraft.factions.quests.inventories.content.SlotPos;
import com.massivecraft.factions.quests.managers.QuestManager;
import com.massivecraft.factions.quests.utils.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class JoinTypeMenuProvider implements InventoryProvider {

    private Faction faction;
    private QuestType questType;

    public JoinTypeMenuProvider(Faction faction, QuestType questType) {
        this.faction = faction;
        this.questType = questType;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
    }

    @Override
    public void update(Player player, InventoryContents contents) {
        contents.set(new SlotPos(JoinTypeMenuConfig.RANKING_ROW, JoinTypeMenuConfig.RANKING_COLUMN), ClickableItem.of(new ItemBuilder(JoinTypeMenuConfig.RANKING_ITEM).setName(ChatColor.translateAlternateColorCodes('&', JoinTypeMenuConfig.JOIN_ITEM_NAME.replace("{category}", questType.getName()))).setLore(JoinTypeMenuConfig.getRankingLore(questType)).toItemStack(), inventoryClickEvent -> inventoryClickEvent.setCancelled(true)));

        contents.set(new SlotPos(JoinTypeMenuConfig.JOIN_ROW, JoinTypeMenuConfig.JOIN_COLUMN), ClickableItem.of(new ItemBuilder(JoinTypeMenuConfig.JOIN_ITEM).setName(ChatColor.translateAlternateColorCodes('&', JoinTypeMenuConfig.JOIN_ITEM_NAME.replace("{category}", questType.getName()))).setLore(ChatColor.translateAlternateColorCodes('&', (QuestManager.INSTANCE.getProfileByName(faction.getTag()) == null ? JoinTypeMenuConfig.JOIN_NORMAL_LORE : QuestManager.INSTANCE.getProfileByName(faction.getTag()).getQuestType() == questType ? JoinTypeMenuConfig.JOIN_ALREADY_IN_THIS_CATEGORY_LORE : JoinTypeMenuConfig.JOIN_ALREADY_IN_A_CATEGORY_LORE)).replace("{category}", questType.getName())).toItemStack(), inventoryClickEvent -> {
            inventoryClickEvent.setCancelled(true);

            if(QuestManager.INSTANCE.getProfileByName(faction.getTag()) != null) return;
            QuestManager.INSTANCE.createProfile(getFaction(), getQuestType());
        }));
    }

    public QuestType getQuestType() {
        return questType;
    }

    public Faction getFaction() {
        return faction;
    }

    public static SmartInventory getInventory(Faction faction, QuestType type) {
        return SmartInventory.builder().size(JoinTypeMenuConfig.ROWS, 9).id("joinMenu").provider(new JoinTypeMenuProvider(faction, type)).title(ChatColor.translateAlternateColorCodes('&', JoinTypeMenuConfig.INVENTORY_NAME.replace("{category}", type.getName()))).build();
    }
}
