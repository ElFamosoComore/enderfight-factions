package com.massivecraft.factions.quests.commands.inventories;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.P;
import com.massivecraft.factions.quests.config.MainMenuConfig;
import com.massivecraft.factions.quests.enums.QuestType;
import com.massivecraft.factions.quests.inventories.ClickableItem;
import com.massivecraft.factions.quests.inventories.SmartInventory;
import com.massivecraft.factions.quests.inventories.content.InventoryContents;
import com.massivecraft.factions.quests.inventories.content.InventoryProvider;
import com.massivecraft.factions.quests.inventories.content.SlotPos;
import com.massivecraft.factions.quests.utils.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MainMenuProvider implements InventoryProvider {

    private FPlayer fPlayer;

    public MainMenuProvider(FPlayer fPlayer) {
        this.fPlayer = fPlayer;
    }

    @Override
    public void init(Player player, InventoryContents contents) {}

    @Override
    public void update(Player player, InventoryContents contents) {
        contents.set(new SlotPos(MainMenuConfig.MINER_ROW, MainMenuConfig.MINER_COLUMN), ClickableItem.of(new ItemBuilder(MainMenuConfig.MINER_ITEM).setLore(MainMenuConfig.getFormattedMinerLore()).setName(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.MINER_ITEM_NAME)).toItemStack(), inventoryClickEvent -> {
            inventoryClickEvent.setCancelled(true);
            JoinTypeMenuProvider.getInventory(fPlayer.getFaction(), QuestType.MINER).open(player);
        }));

        contents.set(new SlotPos(MainMenuConfig.FARM_ROW, MainMenuConfig.FARM_COLUMN), ClickableItem.of(new ItemBuilder(MainMenuConfig.FARM_ITEM).setLore(MainMenuConfig.getFormattedFarmLore()).setName(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.FARM_ITEM_NAME)).toItemStack(), inventoryClickEvent -> {
            inventoryClickEvent.setCancelled(true);
            JoinTypeMenuProvider.getInventory(fPlayer.getFaction(), QuestType.FARMER).open(player);
        }));

        contents.set(new SlotPos(MainMenuConfig.HUNT_ROW, MainMenuConfig.HUNT_COLUMN), ClickableItem.of(new ItemBuilder(MainMenuConfig.HUNT_ITEM).setLore(MainMenuConfig.getFormattedHuntLore()).setName(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.HUNT_ITEM_NAME)).toItemStack(), inventoryClickEvent -> {
            JoinTypeMenuProvider.getInventory(fPlayer.getFaction(), QuestType.HUNTER).open(player);
            inventoryClickEvent.setCancelled(true);
        }));
    }

    public static SmartInventory getInventory(FPlayer fPlayer) {
        return SmartInventory.builder().size(MainMenuConfig.ROWS, 9).id("mainMenu").provider(new MainMenuProvider(fPlayer)).title(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.INVENTORY_NAME)).build();
    }
}
