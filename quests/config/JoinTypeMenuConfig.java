package com.massivecraft.factions.quests.config;

import com.google.common.collect.Lists;
import com.massivecraft.factions.quests.enums.QuestType;
import com.massivecraft.factions.quests.managers.QuestManager;
import com.massivecraft.factions.quests.profiles.QuestProfile;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JoinTypeMenuConfig {
    /*
    Ici, vous pourrez modifier l'emplacement, le materiau utilisé de tous les items, la taille de l'inventaire, le nom, ...
    */

    //GENERAL
    public static final int ROWS = 1;
    public static final String INVENTORY_NAME = "&6Quêtes&8: &e{category}";

    //CLASSEMENT
    public static final ItemStack RANKING_ITEM = new ItemStack(Material.PAPER);
    public static final String RANKING_ITEM_NAME = "&cClassement";
    public static final String FIRST_LORE_LINE = "&cClassement de la catégorie {category}: ";
    public static final String LIST_LORE_LINES = "&8- &6{faction}&7: &e{points} points";
    public static final int RANKING_ROW = 0;
    public static final int RANKING_COLUMN = 2;

    //JOIN
    public static final ItemStack JOIN_ITEM = new ItemStack(Material.PAPER);
    public static final String JOIN_ITEM_NAME = "&cRejoindre la catégorie {category}";
    public static final String JOIN_NORMAL_LORE = "&aCliquez pour rejoindre !";
    public static final String JOIN_ALREADY_IN_THIS_CATEGORY_LORE = "&cVous êtes déjà dans cette catégorie !";
    public static final String JOIN_ALREADY_IN_A_CATEGORY_LORE = "&cVous êtes déjà dans une catégorie !";
    public static final int JOIN_ROW = 0;
    public static final int JOIN_COLUMN = 6;

    public static String[] getRankingLore(QuestType type) {
        ArrayList<String> lore = Lists.newArrayList();

        lore.add(ChatColor.translateAlternateColorCodes('&', FIRST_LORE_LINE).replace("{category}", type.getName()));

        List<QuestProfile> profiles = QuestManager.INSTANCE.getProfiles().stream().filter(questProfile -> questProfile.getQuestType() == type).collect(Collectors.toList());

        for(int i = 0; i < 10; i++) {
            if(i+1 > profiles.size()) {
                lore.add(ChatColor.translateAlternateColorCodes('&', LIST_LORE_LINES).replace("{faction}", "/").replace("{points}", "/"));
                continue;
            }
            lore.add(ChatColor.translateAlternateColorCodes('&', LIST_LORE_LINES).replace("{faction}", profiles.get(i).getFactionName()).replace("{points}", profiles.get(i).getPoints()+""));
        }

        return lore.toArray(new String[0]);
    }
}
