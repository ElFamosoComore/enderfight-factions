package com.massivecraft.factions.quests.config;

import com.google.common.collect.Lists;
import com.massivecraft.factions.quests.enums.QuestType;
import com.massivecraft.factions.quests.managers.QuestManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class MainMenuConfig {
    /*
    Ici, vous pourrez modifier l'emplacement, le materiau utilisé de tous les items, la taille de l'inventaire, le nom, ...
    */

    //GENERAL
    public static final int ROWS = 1;
    public static final String INVENTORY_NAME = "&6Quêtes";

    //MESSAGE
    public static final String RECEIVE_POINTS = "&6Bravo! Vous avez fait gagner 1 point à votre équipe !";

    //ITEMS
    public static final ItemStack MINER_ITEM = new ItemStack(Material.DIAMOND_PICKAXE);
    public static final String MINER_ITEM_NAME = "&cMineur";
    public static final int MINER_ROW = 0;
    public static final int MINER_COLUMN = 0;
    public static final String[] MINER_LORE = new String[] {
            "&cFactions dans cette catégorie: {factions_number}",
            "&cLa quête journalière est : {quest_name}",
            "&cElle se termine dans: {time}"
    };


    public static final ItemStack FARM_ITEM = new ItemStack(Material.DIAMOND_HOE);
    public static final String FARM_ITEM_NAME = "&cFarmeur";
    public static final int FARM_ROW = 0;
    public static final int FARM_COLUMN = 4;
    public static final String[] FARM_LORE = new String[] {
            "&cFactions dans cette catégorie: {factions_number}",
            "&cLa quête journalière est : {quest_name}",
            "&cElle se termine dans: {time}"
    };

    public static final ItemStack HUNT_ITEM = new ItemStack(Material.DIAMOND_SWORD);
    public static final String HUNT_ITEM_NAME = "&cChasseur";
    public static final int HUNT_ROW = 0;
    public static final int HUNT_COLUMN = 8;
    public static final String[] HUNT_LORE = new String[] {
            "&cFactions dans cette catégorie: {factions_number}",
            "&cLa quête journalière est : {quest_name}",
            "&cElle se termine dans: {time}"
    };

    /*
    * NE PAS TOUCHER
    * */

    public static String[] getFormattedMinerLore() {
        ArrayList<String> loreList = Lists.newArrayList();

        for(String s : MINER_LORE) {
            loreList.add(ChatColor.translateAlternateColorCodes('&', s)
                    .replace("{factions_number}", QuestManager.INSTANCE.getProfiles().stream().filter(questProfile -> questProfile.getQuestType() == QuestType.MINER).count()+"")
                    .replace("{quest_name}", QuestManager.INSTANCE.getMinerDailyQuest().getActionType().getName() + " le plus possible de " + QuestManager.INSTANCE.getMinerDailyQuest().getMaterial().toString()));
        }

        return loreList.toArray(new String[0]);
    }

    public static String[] getFormattedFarmLore() {
        ArrayList<String> loreList = Lists.newArrayList();

        for(String s : FARM_LORE) {
            loreList.add(ChatColor.translateAlternateColorCodes('&', s)
                    .replace("{factions_number}", QuestManager.INSTANCE.getProfiles().stream().filter(questProfile -> questProfile.getQuestType() == QuestType.FARMER).count()+"")
                    .replace("{quest_name}", QuestManager.INSTANCE.getFarmDailyQuest().getActionType().getName() + " le plus possible de " + QuestManager.INSTANCE.getFarmDailyQuest().getMaterial().toString()));
        }

        return loreList.toArray(new String[0]);
    }

    public static String[] getFormattedHuntLore() {
        ArrayList<String> loreList = Lists.newArrayList();

        for(String s : HUNT_LORE) {
            loreList.add(ChatColor.translateAlternateColorCodes('&', s)
                    .replace("{factions_number}", QuestManager.INSTANCE.getProfiles().stream().filter(questProfile -> questProfile.getQuestType() == QuestType.HUNTER).count()+"")
                    .replace("{quest_name}", QuestManager.INSTANCE.getHuntDailyQuest().getActionType().getName() + " le plus possible de " + (QuestManager.INSTANCE.getHuntDailyQuest().getMaterial() == null ? QuestManager.INSTANCE.getHuntDailyQuest().getEntityType().toString() : QuestManager.INSTANCE.getHuntDailyQuest().getEntityType() == null ? QuestManager.INSTANCE.getHuntDailyQuest().getMaterial().toString() : QuestManager.INSTANCE.getHuntDailyQuest().getEntityType().toString() + " avec un(e) " + QuestManager.INSTANCE.getHuntDailyQuest().getMaterial().toString())));
        }

        return loreList.toArray(new String[0]);
    }
}
