package com.massivecraft.factions.quests.enums;

public enum ActionType {
    MINE("Miner"),
    CRAFT("Craft"),
    SELL("Vendre"),
    FETCH("Récupérer"),
    KILL("Tuer");

    private String name;

    ActionType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
