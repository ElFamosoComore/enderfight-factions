package com.massivecraft.factions.quests.enums;

import org.bukkit.Material;

import java.util.Arrays;

public enum FarmQuests {
    FETCH_SUGAR_CANE(1, ActionType.FETCH, 1, Material.SUGAR_CANE),
    FETCH_MELON(2, ActionType.FETCH, 1, Material.MELON),
    FETCH_CACAO(3, ActionType.FETCH, 1, Material.COCOA),
    FETCH_PUMPKIN(4, ActionType.FETCH, 1, Material.PUMPKIN),
    FETCH_LILY_PADS(5, ActionType.FETCH, 1, Material.LILY_PAD),
    FETCH_CACTUS(6, ActionType.FETCH, 1, Material.CACTUS),
    FETCH_NETHER_WART(7, ActionType.FETCH, 1, Material.NETHER_WART),
    FETCH_WHEAT(8, ActionType.FETCH, 1, Material.WHEAT),
    FETCH_MELON_BLOCK(9, ActionType.FETCH, 1, Material.LEGACY_MELON_BLOCK),
    FETCH_EGG(10, ActionType.FETCH, 1, Material.EGG),
    FETCH_WHEAT_SEEDS(11, ActionType.FETCH, 1, Material.WHEAT_SEEDS),
    FETCH_POTATO(12, ActionType.FETCH, 1, Material.POTATO),
    FETCH_CARROT(13, ActionType.FETCH, 1, Material.CARROT),
    FETCH_RED_MUSHROOM(14, ActionType.FETCH, 1, Material.RED_MUSHROOM),
    FETCH_OAK_LEAVES(15, ActionType.FETCH, 1, Material.OAK_LEAVES),
    FETCH_BIRCH_LEAVES(16, ActionType.FETCH, 1, Material.BIRCH_LEAVES),
    FETCH_OAK_SAPLING(17, ActionType.FETCH, 1, Material.OAK_SAPLING),
    FETCH_BIRCH_SAPLING(18, ActionType.FETCH, 1, Material.BIRCH_SAPLING),
    FETCH_DARK_OAK_SAPLING(19, ActionType.FETCH, 1, Material.DARK_OAK_SAPLING),
    FETCH_DARK_OAK_LEAVES(20, ActionType.FETCH, 1, Material.DARK_OAK_LEAVES),
    FETCH_APPLES(21, ActionType.FETCH, 1, Material.APPLE),
    CRAFT_COOKIES(22, ActionType.CRAFT, 1, Material.COOKIE),
    CRAFT_BREAD(23, ActionType.CRAFT, 1, Material.BREAD),
    CRAFT_CAKE(24, ActionType.CRAFT, 1, Material.CAKE),
    CRAFT_EYE_OF_ENDER(25, ActionType.CRAFT, 1, Material.ENDER_EYE),
    CRAFT_PUMPKIN_PIE(26, ActionType.CRAFT, 1, Material.PUMPKIN_PIE),
    CRAFT_BEACON(27, ActionType.CRAFT, 1, Material.BEACON),
    CRAFT_HAY_BLOCK(28, ActionType.CRAFT, 1, Material.HAY_BLOCK),
    CRAFT_BOOKSHELF(29, ActionType.CRAFT, 1, Material.BOOKSHELF),
    FETCH_FLOWER(30, ActionType.FETCH, 1, Material.LEGACY_YELLOW_FLOWER);

    private int day;
    private ActionType actionType;
    private long pointsPerUnit;
    private Material material;

    FarmQuests(int day, ActionType actionType, long pointsPerUnit, Material material) {
        this.day = day;
        this.actionType = actionType;
        this.pointsPerUnit = pointsPerUnit;
        this.material = material;
    }

    public int getDay() {
        return day;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public long getPointsPerUnit() {
        return pointsPerUnit;
    }

    public Material getMaterial() {
        return material;
    }


    public static FarmQuests getByDay(int day) {
        return Arrays.stream(values()).filter(quests -> quests.getDay() == day).findFirst().orElse(null);
    }
}
