package com.massivecraft.factions.quests.enums;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import java.util.Arrays;

public enum HuntQuests {
    KILL_COW(1, ActionType.KILL, 1, EntityType.COW),
    KILL_SHEEP(2, ActionType.KILL, 1, EntityType.SHEEP),
    KILL_PIG(3, ActionType.KILL, 1, EntityType.PIG),
    KILL_CHICKEN(4, ActionType.KILL, 1, EntityType.CHICKEN),
    KILL_SQUID(5, ActionType.KILL, 1, EntityType.SQUID),
    KILL_ZOMBIE(6, ActionType.KILL, 1, EntityType.ZOMBIE),
    KILL_SKELETON(7, ActionType.KILL, 1, EntityType.SKELETON),
    KILL_BLAZE(8, ActionType.KILL, 1, EntityType.BLAZE),
    KILL_ENDERMAN(9, ActionType.KILL, 1, EntityType.ENDERMAN),
    KILL_CREEPER(10, ActionType.KILL, 1, EntityType.CREEPER),
    KILL_SPIDER(11, ActionType.KILL, 1, EntityType.SPIDER),
    KILL_CAVE_SPIDER(12, ActionType.KILL, 1, EntityType.CAVE_SPIDER),
    KILL_PLAYER_WITH_DIAMOND_SWORD(13, ActionType.KILL, 1, Material.DIAMOND_SWORD, EntityType.PLAYER),
    KILL_PLAYER_WITH_BOW(14, ActionType.KILL, 1, Material.BOW, EntityType.PLAYER),
    KILL_PLAYER_WITH_AXE(15, ActionType.KILL, 1, Material.DIAMOND_AXE, EntityType.PLAYER),
    KILL_PLAYER_WITH_IRON_SWORD(16, ActionType.KILL, 1, Material.IRON_SWORD, EntityType.PLAYER),
    FETCH_BONE(17, ActionType.FETCH, 1, Material.BONE),
    FETCH_ROTTEN_FLESH(18, ActionType.FETCH, 1, Material.ROTTEN_FLESH),
    FETCH_LEATHER(19, ActionType.FETCH, 1, Material.LEATHER),
    FETCH_BEEF(20, ActionType.FETCH, 1, Material.BEEF),
    FETCH_RAW_CHICKEN(21, ActionType.FETCH, 1, Material.LEGACY_RAW_CHICKEN),
    KILL_RAW_MUTTON(22, ActionType.KILL, 1, EntityType.BAT),
    FETCH_SPIDER_EYE(23, ActionType.FETCH, 1, Material.SPIDER_EYE),
    FETCH_SLIME_BALL(24, ActionType.FETCH, 1, Material.SLIME_BALL),
    FETCH_ENDERPEARL(25, ActionType.FETCH, 1, Material.ENDER_PEARL),
    FETCH_GUNPOWDER(26, ActionType.FETCH, 1, Material.GUNPOWDER),
    FETCH_INK_SAC(27, ActionType.FETCH, 1, Material.INK_SAC),
    FETCH_STRING(28, ActionType.FETCH, 1, Material.STRING),
    FETCH_RABBIT_HIDE(29, ActionType.FETCH, 1, Material.RABBIT_HIDE),
    KILL_NETHER_SKELETON(30, ActionType.KILL, 1, EntityType.BAT);

    private int day;
    private ActionType actionType;
    private long pointsPerUnit;
    private Material material;
    private EntityType entityType;

    HuntQuests(int day, ActionType actionType, long pointsPerUnit, EntityType entityType) {
        this.day = day;
        this.actionType = actionType;
        this.pointsPerUnit = pointsPerUnit;
        this.entityType = entityType;
    }

    HuntQuests(int day, ActionType actionType, long pointsPerUnit, Material material) {
        this.day = day;
        this.actionType = actionType;
        this.pointsPerUnit = pointsPerUnit;
        this.material = material;
    }

    HuntQuests(int day, ActionType actionType, long pointsPerUnit, Material material, EntityType entityType) {
        this.day = day;
        this.actionType = actionType;
        this.pointsPerUnit = pointsPerUnit;
        this.material = material;
        this.entityType = entityType;
    }

    public int getDay() {
        return day;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public long getPointsPerUnit() {
        return pointsPerUnit;
    }

    public Material getMaterial() {
        return material;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public static HuntQuests getByDay(int day) {
        return Arrays.stream(values()).filter(quests -> quests.getDay() == day).findFirst().orElse(null);
    }
}
