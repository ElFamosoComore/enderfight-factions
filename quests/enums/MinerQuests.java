package com.massivecraft.factions.quests.enums;

import org.bukkit.Material;

import java.util.Arrays;

public enum MinerQuests {
    MINE_IRON(1, ActionType.MINE, 1, Material.IRON_ORE),
    MINE_OBSIDIAN(2, ActionType.MINE, 1, Material.OBSIDIAN),
    MINE_COBBLESTONE(3, ActionType.MINE, 1, Material.COBBLESTONE),
    MINE_EMERALD(4, ActionType.MINE, 1, Material.EMERALD_ORE),
    MINE_DIAMOND(5, ActionType.MINE, 1, Material.DIAMOND_ORE),
    MINE_COAL(6, ActionType.MINE, 1, Material.COAL_ORE),
    MINE_REDSTONE(7, ActionType.MINE, 1, Material.REDSTONE_ORE),
    MINE_LAPIS(8, ActionType.MINE, 1, Material.LAPIS_ORE),
    MINE_GOLD(9, ActionType.MINE, 1, Material.GOLD_ORE),
    MINE_QUARTZ(10, ActionType.MINE, 1, Material.NETHER_QUARTZ_ORE),
    MINE_NETHERRACK(11, ActionType.MINE, 1, Material.NETHERRACK),
    MINE_SAND(12, ActionType.MINE, 1, Material.SAND),
    MINE_OBSIDIAN2(13, ActionType.MINE, 1, Material.OBSIDIAN),
    MINE_GRAVEL(14, ActionType.MINE, 1, Material.GRAVEL),
    CRAFT_IRON_BLOCK(15, ActionType.CRAFT, 1, Material.IRON_BLOCK),
    CRAFT_GOLD_BLOCK(16, ActionType.CRAFT, 1, Material.GOLD_BLOCK),
    CRAFT_DIAMOND_BLOCK(17, ActionType.CRAFT, 1, Material.DIAMOND_BLOCK),
    CRAFT_EMERALD_BLOCK(18, ActionType.CRAFT, 1, Material.EMERALD_BLOCK),
    CRAFT_COAL_BLOCK(19, ActionType.CRAFT, 1, Material.COAL_BLOCK),
    CRAFT_REDSTONE_BLOCK(20, ActionType.CRAFT, 1, Material.REDSTONE_BLOCK),
    CRAFT_LAPIS_BLOCK(21, ActionType.CRAFT, 1, Material.LAPIS_BLOCK),
    SELL_IRON(22, ActionType.SELL, 1, Material.IRON_INGOT),
    SELL_GOLD(23, ActionType.SELL, 1, Material.GOLD_INGOT),
    SELL_COAL(24, ActionType.SELL, 1, Material.COAL),
    SELL_REDSTONE(25, ActionType.SELL, 1, Material.REDSTONE),
    SELL_LAPIS(26, ActionType.SELL, 1, Material.LAPIS_LAZULI),
    FETCH_ANDESITE(28, ActionType.FETCH, 1, Material.ANDESITE),
    FETCH_DIORITE(29, ActionType.FETCH, 1, Material.DIORITE),
    FETCH_OAK_WOOD(30, ActionType.FETCH, 1, Material.OAK_WOOD);

    private int day;
    private ActionType actionType;
    private long pointsPerUnit;
    private Material material;

    MinerQuests(int day, ActionType actionType, long pointsPerUnit, Material material) {
        this.day = day;
        this.actionType = actionType;
        this.pointsPerUnit = pointsPerUnit;
        this.material = material;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public long getPointsPerUnit() {
        return pointsPerUnit;
    }

    public Material getMaterial() {
        return material;
    }

    public int getDay() {
        return day;
    }

    public static MinerQuests getByDay(int day) {
        return Arrays.stream(values()).filter(quests -> quests.getDay() == day).findFirst().orElse(null);
    }
}
