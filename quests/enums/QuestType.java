package com.massivecraft.factions.quests.enums;

public enum QuestType {
    MINER(1, "Mineur", MinerQuests.class),
    FARMER(2, "Farmeur", FarmQuests.class),
    HUNTER(3, "Chasseur", HuntQuests.class);

    private int id;
    private String name;
    private Class<? extends Enum> anEnum;

    QuestType(int id, String name, Class<? extends Enum> anEnum) {
        this.id = id;
        this.name = name;
        this.anEnum = anEnum;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Class<? extends Enum> getEnum() {
        return anEnum;
    }
}
