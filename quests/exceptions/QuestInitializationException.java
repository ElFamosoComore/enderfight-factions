package com.massivecraft.factions.quests.exceptions;

public class QuestInitializationException extends Exception {
    public QuestInitializationException(Exception e) {
        super(e.getClass().getSimpleName()+": "+e.getMessage());
    }
}
