package com.massivecraft.factions.quests.exceptions;

public class QuestLoadException extends Exception {
    public QuestLoadException(Exception e) {
        super(e.getClass().getSimpleName()+": "+e.getMessage());
    }
}
