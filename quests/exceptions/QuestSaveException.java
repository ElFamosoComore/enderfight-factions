package com.massivecraft.factions.quests.exceptions;

public class QuestSaveException extends Exception {
    public QuestSaveException(Exception e) {
        super(e.getClass().getSimpleName()+": "+e.getMessage());
    }
}
