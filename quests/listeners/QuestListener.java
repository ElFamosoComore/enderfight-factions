package com.massivecraft.factions.quests.listeners;

import com.massivecraft.factions.*;
import com.massivecraft.factions.event.FactionDisbandEvent;
import com.massivecraft.factions.quests.config.MainMenuConfig;
import com.massivecraft.factions.quests.enums.ActionType;
import com.massivecraft.factions.quests.enums.FarmQuests;
import com.massivecraft.factions.quests.enums.HuntQuests;
import com.massivecraft.factions.quests.enums.MinerQuests;
import com.massivecraft.factions.quests.managers.QuestManager;
import com.massivecraft.factions.quests.profiles.QuestProfile;
import fr.enderfight.core.shops.events.ShopAction;
import fr.enderfight.core.shops.events.ShopEvent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class QuestListener implements Listener {
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player breaker = event.getPlayer();
        Block block = event.getBlock();

        Faction faction = FPlayers.getInstance().getByPlayer(breaker).getFaction();
        if(faction == null) return;
        if(faction.isWilderness()) return;

        ActionType actionType = ActionType.MINE;

        if(QuestManager.INSTANCE.getProfileByName(faction.getTag()) == null) return;

        QuestProfile questProfile = QuestManager.INSTANCE.getProfileByName(faction.getTag());


        switch (questProfile.getQuestType()) {
            case MINER:
                MinerQuests minerQuests = QuestManager.INSTANCE.getMinerDailyQuest();

                if(actionType != minerQuests.getActionType()) return;

                Material material = minerQuests.getMaterial();

                if(material != block.getType()) return;

                questProfile.addPoints(minerQuests.getPointsPerUnit());
                breaker.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;

            case FARMER:
                FarmQuests farmQuests = QuestManager.INSTANCE.getFarmDailyQuest();

                if(actionType != farmQuests.getActionType()) return;

                Material farmMaterial = farmQuests.getMaterial();

                if(farmMaterial != block.getType()) return;

                questProfile.addPoints(farmQuests.getPointsPerUnit());
                breaker.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;

            case HUNTER:
                HuntQuests huntQuests = QuestManager.INSTANCE.getHuntDailyQuest();

                if(actionType != huntQuests.getActionType()) return;

                Material huntMaterial = huntQuests.getMaterial();

                if(huntMaterial != block.getType()) return;

                questProfile.addPoints(huntQuests.getPointsPerUnit());
                breaker.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;
        }
    }

    @EventHandler
    public void onPlayerPickItem(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItem().getItemStack();

        Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();

        if(faction == null) return;
        if(faction.isWilderness()) return;

        ActionType actionType = ActionType.FETCH;

        if(QuestManager.INSTANCE.getProfileByName(faction.getTag()) == null) return;

        QuestProfile questProfile = QuestManager.INSTANCE.getProfileByName(faction.getTag());

        switch (questProfile.getQuestType()) {
            case MINER:
                MinerQuests minerQuests = QuestManager.INSTANCE.getMinerDailyQuest();

                if(minerQuests == null) return;

                if(actionType != minerQuests.getActionType()) return;

                Material material = minerQuests.getMaterial();

                if(material != itemStack.getType()) return;

                questProfile.addPoints(minerQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;

            case FARMER:
                FarmQuests farmQuests = QuestManager.INSTANCE.getFarmDailyQuest();

                if(farmQuests == null) return;

                if(actionType != farmQuests.getActionType()) return;

                Material farmMaterial = farmQuests.getMaterial();

                if(farmMaterial != itemStack.getType()) return;

                questProfile.addPoints(farmQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;

            case HUNTER:
                HuntQuests huntQuests = QuestManager.INSTANCE.getHuntDailyQuest();

                if(huntQuests == null) return;

                if(actionType != huntQuests.getActionType()) return;

                Material huntMaterial = huntQuests.getMaterial();

                if(huntMaterial != itemStack.getType()) return;

                questProfile.addPoints(huntQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;
        }
    }

    @EventHandler
    public void onPlayerCraftItem(CraftItemEvent event) {
        if(!(event.getView().getPlayer() instanceof Player)) return;

        Player player = (Player) event.getView().getPlayer();
        ItemStack itemStack = event.getRecipe().getResult();

        Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();

        if(faction == null) return;
        if(faction.isWilderness()) return;

        ActionType actionType = ActionType.FETCH;

        if(QuestManager.INSTANCE.getProfileByName(faction.getTag()) == null) return;

        QuestProfile questProfile = QuestManager.INSTANCE.getProfileByName(faction.getTag());

        switch (questProfile.getQuestType()) {
            case MINER:
                MinerQuests minerQuests = QuestManager.INSTANCE.getMinerDailyQuest();

                if(minerQuests == null) return;

                if(actionType != minerQuests.getActionType()) return;

                Material material = minerQuests.getMaterial();

                if(material != itemStack.getType()) return;

                questProfile.addPoints(minerQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;

            case FARMER:
                FarmQuests farmQuests = QuestManager.INSTANCE.getFarmDailyQuest();

                if(farmQuests == null) return;

                if(actionType != farmQuests.getActionType()) return;

                Material farmMaterial = farmQuests.getMaterial();

                if(farmMaterial != itemStack.getType()) return;

                questProfile.addPoints(farmQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;

            case HUNTER:
                HuntQuests huntQuests = QuestManager.INSTANCE.getHuntDailyQuest();

                if(huntQuests == null) return;

                if(actionType != huntQuests.getActionType()) return;

                Material huntMaterial = huntQuests.getMaterial();

                if(huntMaterial != itemStack.getType()) return;

                questProfile.addPoints(huntQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;
        }
    }

    @EventHandler
    public void onEntityKillEvent(EntityDeathEvent event) {
        EntityType entityType = event.getEntityType();

        if(event.getEntity().getKiller() == null) return;

        Player player = event.getEntity().getKiller();

        Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();

        if(faction == null) return;
        if(faction.isWilderness()) return;

        ActionType actionType = ActionType.KILL;

        if(QuestManager.INSTANCE.getProfileByName(faction.getTag()) == null) return;

        QuestProfile questProfile = QuestManager.INSTANCE.getProfileByName(faction.getTag());
        switch (questProfile.getQuestType()) {
            case HUNTER:
                HuntQuests huntQuests = QuestManager.INSTANCE.getHuntDailyQuest();

                if(huntQuests == null) return;

                if(actionType != huntQuests.getActionType()) return;

                if(huntQuests.getEntityType() != entityType) return;

                if(huntQuests.getMaterial() != null) {
                    if(player.getItemInHand().getType() != huntQuests.getMaterial()) return;
                }

                questProfile.addPoints(huntQuests.getPointsPerUnit());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                break;
        }
    }

    @EventHandler
    public void onFactionDisband(FactionDisbandEvent event) {
        QuestManager.INSTANCE.deleteProfile(event.getFaction());
    }

    @EventHandler
    public void onShopEvent(ShopEvent event) {
        if(event.getAction() == ShopAction.SELL) {
            Player player = (Player) event.getPlayer();
            ItemStack itemStack = event.getItem();

            Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();

            if(faction == null) return;
            if(faction.isWilderness()) return;

            ActionType actionType = ActionType.SELL;

            if(QuestManager.INSTANCE.getProfileByName(faction.getTag()) == null) return;

            QuestProfile questProfile = QuestManager.INSTANCE.getProfileByName(faction.getTag());

            switch (questProfile.getQuestType()) {
                case MINER:
                    MinerQuests minerQuests = QuestManager.INSTANCE.getMinerDailyQuest();

                    if(minerQuests == null) return;

                    if(actionType != minerQuests.getActionType()) return;

                    Material material = minerQuests.getMaterial();

                    if(material != itemStack.getType()) return;

                    questProfile.addPoints(minerQuests.getPointsPerUnit());
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                    break;

                case FARMER:
                    FarmQuests farmQuests = QuestManager.INSTANCE.getFarmDailyQuest();

                    if(farmQuests == null) return;

                    if(actionType != farmQuests.getActionType()) return;

                    Material farmMaterial = farmQuests.getMaterial();

                    if(farmMaterial != itemStack.getType()) return;

                    questProfile.addPoints(farmQuests.getPointsPerUnit());
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                    break;

                case HUNTER:
                    HuntQuests huntQuests = QuestManager.INSTANCE.getHuntDailyQuest();

                    if(huntQuests == null) return;

                    if(actionType != huntQuests.getActionType()) return;

                    Material huntMaterial = huntQuests.getMaterial();

                    if(huntMaterial != itemStack.getType()) return;

                    questProfile.addPoints(huntQuests.getPointsPerUnit());
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', MainMenuConfig.RECEIVE_POINTS));
                    break;
            }
        }
    }
}
