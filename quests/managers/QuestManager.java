package com.massivecraft.factions.quests.managers;

import com.google.common.collect.Lists;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.P;
import com.massivecraft.factions.quests.enums.FarmQuests;
import com.massivecraft.factions.quests.enums.HuntQuests;
import com.massivecraft.factions.quests.enums.MinerQuests;
import com.massivecraft.factions.quests.enums.QuestType;
import com.massivecraft.factions.quests.exceptions.QuestInitializationException;
import com.massivecraft.factions.quests.exceptions.QuestLoadException;
import com.massivecraft.factions.quests.exceptions.QuestSaveException;
import com.massivecraft.factions.quests.inventories.InventoryManager;
import com.massivecraft.factions.quests.listeners.QuestListener;
import com.massivecraft.factions.quests.profiles.QuestProfile;
import com.massivecraft.factions.quests.tasks.SortTask;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class QuestManager {

    public static QuestManager INSTANCE;
    private InventoryManager inventoryManager;

    private Logger logger = Bukkit.getServer().getLogger();
    private List<QuestProfile> profiles;
    private long firstDay;

    public QuestManager() throws QuestInitializationException {
        init();
    }

    private void init() throws QuestInitializationException {
        try {
            INSTANCE = this;

            profiles = Lists.newArrayList();

            loadProfiles();

            this.firstDay = getFirstDay();

            this.inventoryManager = new InventoryManager();

            inventoryManager.init();

            Bukkit.getPluginManager().registerEvents(new QuestListener(), P.p);

            new SortTask().runTaskTimer(P.p, 1, 30*60*1000);
        } catch (Exception e) {
            throw new QuestInitializationException(e);
        }
    }

    private long getFirstDay() throws IOException {
        File questConfigFile = new File(P.p.getDataFolder(), ".quest_do_not_touch");

        if(!questConfigFile.exists()) questConfigFile.createNewFile();

        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(questConfigFile);

        if(!yamlConfiguration.isSet("firstDay")) {
            yamlConfiguration.set("firstDay", System.currentTimeMillis());
        }

        yamlConfiguration.save(questConfigFile);

        return yamlConfiguration.getLong("firstDay");
    }

    public void loadProfiles() throws QuestLoadException {
        try {
            File profilesFolder = new File(P.p.getDataFolder(), "profiles");

            if(!profilesFolder.exists()) profilesFolder.mkdirs();

            if(profilesFolder.listFiles().length == 0) return;

            for(File file : profilesFolder.listFiles()) {
                if(file.isDirectory()) continue;

                QuestProfile questProfile = P.p.gson.fromJson(new String(Files.readAllBytes(Paths.get(file.getCanonicalPath()))), QuestProfile.class);

                getProfiles().add(questProfile);
                getLogger().info("Registered " + questProfile.getFactionName() + " profile!");
            }
        } catch (Exception e) {
            throw new QuestLoadException(e);
        }
    }

    public void saveProfiles() throws QuestSaveException {
        try {
            File profilesFolder = new File(P.p.getDataFolder(), "profiles");

            if(!profilesFolder.exists()) profilesFolder.mkdirs();

            for (QuestProfile profile : getProfiles()) {
                File profileFile = new File(profilesFolder, profile.getFactionName()+".json");

                if(!profileFile.exists()) profileFile.createNewFile();

                FileWriter fileWriter = new FileWriter(profileFile);
                fileWriter.write(P.p.gson.toJson(profile));
                fileWriter.close();
                getLogger().info("Saved" + profile.getFactionName() + " profile!");
            }
        } catch (Exception e) {
            throw new QuestSaveException(e);
        }
    }

    public QuestProfile getProfileByName(String factionName) {
        return getProfiles().stream().filter(questProfile -> questProfile.getFactionName().equalsIgnoreCase(factionName)).findFirst().orElse(null);
    }

    public List<QuestProfile> getProfiles() {
        return profiles;
    }

    public Logger getLogger() {
        return logger;
    }

    private static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public int getDay() {
        return (int) getDifferenceDays(new Date(firstDay), new Date());
    }

    public MinerQuests getMinerDailyQuest() {
        return MinerQuests.getByDay(getDay()+1) == null ? MinerQuests.getByDay(setFirstDay()+1) : MinerQuests.getByDay(getDay()+1);
    }

    public FarmQuests getFarmDailyQuest() {
        return FarmQuests.getByDay(getDay()+1) == null ? FarmQuests.getByDay(setFirstDay()+1) : FarmQuests.getByDay(getDay()+1);
    }

    public HuntQuests getHuntDailyQuest() {
        return HuntQuests.getByDay(getDay()+1) == null ? HuntQuests.getByDay(setFirstDay()+1) : HuntQuests.getByDay(getDay()+1);
    }

    public void createProfile(Faction faction, QuestType questType) {
        getProfiles().add(new QuestProfile(faction.getTag(), questType));
    }

    public void deleteProfile(Faction faction) {
        getProfiles().remove(getProfileByName(faction.getTag()));

        File profilesFolder = new File(P.p.getDataFolder(), "profiles");
        File profileFile = new File(profilesFolder, faction.getTag()+".json");

        if(profileFile.exists()) profileFile.delete();
    }

    public int setFirstDay() {
        File questConfigFile = new File(P.p.getDataFolder(), ".quest_do_not_touch");

        if(!questConfigFile.exists()) {
            try {
                questConfigFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(questConfigFile);

        yamlConfiguration.set("firstDay", System.currentTimeMillis());


        yamlConfiguration.save(questConfigFile);
        return 0;
    }

    public InventoryManager getInventoryManager() {
        return inventoryManager;
    }
}
