package com.massivecraft.factions.quests.profiles;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.massivecraft.factions.quests.enums.QuestType;

import java.util.List;
import java.util.Map;

public class QuestProfile {
    private String factionName;
    private QuestType questType;
    private long points;

    public QuestProfile(String factionName, QuestType questType) {
        this.factionName = factionName;
        this.questType = questType;
        this.points = 0;
    }

    public QuestProfile(String factionName, QuestType questType, long points) {
        this.factionName = factionName;
        this.questType = questType;
        this.points = points;
    }

    public String getFactionName() {
        return factionName;
    }

    public QuestType getQuestType() {
        return questType;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public void addPoints(long points) {
        setPoints(getPoints() + points);
    }

    public void removePoints(long points) {
        setPoints(getPoints() - points);
    }
}
