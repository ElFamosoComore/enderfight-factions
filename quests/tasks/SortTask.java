package com.massivecraft.factions.quests.tasks;

import com.massivecraft.factions.quests.managers.QuestManager;
import com.massivecraft.factions.quests.profiles.QuestProfile;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Comparator;

public class SortTask extends BukkitRunnable {
    @Override
    public void run() {
        QuestManager.INSTANCE.getLogger().info("Starting sort task...");
        QuestManager.INSTANCE.getProfiles().sort(Comparator.comparingLong(QuestProfile::getPoints).reversed());
        QuestManager.INSTANCE.getLogger().info("Sort task finished successfully!");
    }
}
